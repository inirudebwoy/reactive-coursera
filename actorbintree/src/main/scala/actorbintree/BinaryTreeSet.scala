/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case Insert(requester, id, elem) => {
      root ! Insert(requester, id, elem)
    }
    case Remove(requester, id, elem) => {
      root ! Remove(requester, id, elem)
    }
    case Contains(requester, id, elem) => {
      root ! Contains(requester, id, elem)
    }
    case GC => {
      var newRoot = createRoot
      // this along with copying
      context.become(garbageCollecting(newRoot))
      root ! CopyTo(newRoot)
    }
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case BinaryTreeNode.CopyFinished => {
      pendingQueue foreach (newRoot ! _)
      pendingQueue = Queue.empty[Operation]
      this.root = newRoot
    }
    case Insert(requester, id, elem) => {
      pendingQueue = pendingQueue :+ Insert(requester, id, elem)
    }
    case Remove(requester, id, elem) => {
      pendingQueue = pendingQueue :+ Remove(requester, id, elem)
    }
    case Contains(requester, id, elem) => {
      pendingQueue = pendingQueue :+ Contains(requester, id, elem)
    }
  }
}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case Insert(requester, id, elem) => {
      // pattern match with guard?
      if (this.elem < elem) {
        if (subtrees contains Right) {
          subtrees(Right) ! Insert(requester, id, elem)
        } else {
          subtrees = subtrees + (BinaryTreeNode.Right -> context.actorOf(BinaryTreeNode.props(elem, false)))
          requester ! OperationFinished(id)
        }
      } else if (this.elem > elem) {
        if (subtrees contains Left) {
          subtrees(Left) ! Insert(requester, id, elem)
        } else {
          subtrees = subtrees + (BinaryTreeNode.Left -> context.actorOf(BinaryTreeNode.props(elem, false)))
          requester ! OperationFinished(id)
        }
      } else {
        this.removed = false
        requester ! OperationFinished(id)
      }
    }

    case Contains(requester, id, elem) => {
      if (this.elem == elem) {
        requester ! ContainsResult(id, !this.removed)
      }
      else if (subtrees.contains(Right) && this.elem < elem) {
        subtrees(Right) ! Contains(requester, id, elem)
      }
      else if (subtrees.contains(Left) && this.elem > elem) {
        subtrees(Left) ! Contains(requester, id, elem)
      }
      else requester ! ContainsResult(id, false)
    }

    case Remove(requester, id, elem) => {
      if (this.elem == elem) {
        this.removed = true
        requester ! OperationFinished(id)
      }
      else if (subtrees.contains(Right) && this.elem < elem) {
        subtrees(Right) ! Remove(requester, id, elem)
      }
      else if (subtrees.contains(Left) && this.elem > elem) {
        subtrees(Left) ! Remove(requester, id, elem)
      }
      else requester ! OperationFinished(id)

    }

    case CopyTo(treeNode) => {
      // does remove node has children?
      // should not
      if (!this.removed) {
        treeNode ! Insert(self, elem, elem)
        subtrees foreach {
          case (pos, ac) => ac ! CopyTo(treeNode)
        }
      }
    }
    // case _ => ???
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = ???
  // {
  //   case BinaryTreeNode.OperationFinished(id) => context.stop(self)
  // }

}

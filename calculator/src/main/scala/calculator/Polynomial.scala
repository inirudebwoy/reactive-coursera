package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal{b() * b() - 4 * a() * c()}
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
    c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {

    def compute(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Set[Double] = delta match {
      case (delta) if delta() < 0 => Set(0)
      case (delta) if delta() == 0 => Set(-b() / (2 * a()))
      case (delta) if delta() > 0 => Set((-b() + delta()) / (2 * a()),
        (-b() - delta()) / (2 * a()))
    }

    Signal {compute(a, b, c, delta)}
  }
}
